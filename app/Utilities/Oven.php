<?php

namespace App\Utilities;

interface Oven
{
    /**
     * Just echo time to heat up
     *
     * @return string
     */
    public function heatUp(): string;

    /**
     * Calculate and echo time to cook
     * Update Pizza status (raw -> cooked and cooked -> overcooked)
     *
     * @param Pizza $pizza
     * @return int
     */
    public function bake(Pizza &$pizza): int;

    /**
     * Just echo 'oven is off'
     *
     * @return string
     */
    public function turnOff(): string;
}
