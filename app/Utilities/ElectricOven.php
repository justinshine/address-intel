<?php

namespace App\Utilities;

use Illuminate\Support\Collection;

class ElectricOven implements Oven
{
    const STATUS_HOT = 'hot';
    const STATUS_COLD = 'cold';

    private $status = '';

    public function heatUp(): string
    {
        $this->status = self::STATUS_HOT;
        return '10 minutes to heat up oven';
    }

    public function bake(Pizza &$pizza): int
    {
        $bake_time = 0; 
        if ($this->status != self::STATUS_HOT) 
        {
            $this->heatUp();
            $bake_time += 10;
        }
        // Five minutes to bake the pizza
        $bake_time += 5; 
        // One minutes for each ingredient
        $ingredient = $pizza->recipe->ingredientRequirements()->get();
        foreach($ingredient as $check) {
            $bake_time += 1; 
        }
        $pizza->setStatus('cooked');
        if($bake_time > 25) {
            $pizza->setStatus('overCooked');    
        }
        return $bake_time;
    }

    public function turnOff(): string
    {
        $this->status = self::STATUS_COLD;
        return 'Oven is now off';
    }

}